const log = require('../logger/logger');
const data = require('./data');
const { getFromCache, setInCache } = require('../cache/redis');
const validUrl = require("valid-url");
const shortid = require("shortid");


let createUrl = async ({userId , url , baseUrl}) => {
    if (!validUrl.isUri(url.longUrl)){
        return Promise.resolve({status:400, body:  {message: 'long url is not valid uri'}})
    }
    if (url.shortUrl && !shortid.isValid(url.shortUrl)){
        delete url.shortUrl;
    }
    let shortUrl = url.shortUrl || shortid.generate();
    let promise = new Promise((resolve, reject)=>{
        data.createUrl({userId , shortUrl , longUrl:url.longUrl}).then((url)=>{
            url = url.toJSON();
            log('info', `url ${url.shortUrl} created`);
            setInCache(shortUrl,{longUrl: url.longUrl, user: url.userId})
            resolve({body:{url: `${baseUrl}/${url.shortUrl}`, shortUrl}, status: 200});
        })
        .catch((err) => {
            log('error', JSON.stringify(err));
            if (err.code === 11000) {
                log('error' , 'duplicate url');
                reject({body: {message: 'duplicate url'}, status: 400});
                return;
            }
            resolve({body: err, status: 422});
        });
    });
    return await promise;
}

let getUrl = async (shortUrl) => {
    let promise = new Promise((resolve, reject)=>{
        getFromCache(shortUrl).then(result =>{
            result = JSON.parse(result);
            resolve({status:301, longUrl: result.longUrl, creator: result.user});
        }).catch(async (err) =>{
            log('error' , err);
            await data.getUrl(shortUrl).then(url=>{
                if (url){
                    url = url.toJSON();
                    setInCache(shortUrl,{longUrl: url.longUrl , user: url.userId})
                    resolve({status:301 , longUrl:url.longUrl , creator: url.userId});
                }
                else{
                    resolve({status: 404, body: {message: 'shortUrl not exist'}});
                }
            }).catch(err=>{
                resolve({status: 400 , body:err});
            })
        })
    });
    return await promise;
}



module.exports = {createUrl, getUrl};