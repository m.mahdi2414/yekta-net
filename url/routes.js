const express = require('express');
const router = express.Router();
const {createUrl, getUrl} = require('./service');
const service = require('../user/service');
const {createVisit} = require('./../visit/service');
const log = require('../logger/logger');

router.use(function (req, res, next) {
	log('info', `new ${req.method} request on ${req.originalUrl}`);
	next();
});

router.post('/', (req, res) =>{
	const userId = req.user.id;
	let url = req.body;
	createUrl({userId ,url , baseUrl: `http://${req.headers.host}/api/urls`}).then(result=>{
		res.status(result.status).json(result.body);
	})
	.catch(err=>{
		if(err.status){
			delete url.shortUrl
			createUrl({userId, url , baseUrl: `http://${req.headers.host}/api/urls`}).then(result=>{
				res.status(result.status).json(result.body);
			})
			.catch(err=>{
				console.log(err);
				res.status(500).json({message: 'interval error'});
			})
		}
		else{
			log('error', err);
			res.status(500).json({message: 'interval error'});
		}
	})
})

router.get('/:shortUrl', (req, res) =>{
	const shortUrl = req.params.shortUrl;
	getUrl(shortUrl).then(result=>{
		if (result.status === 301){
			createVisit({userAgent:req.headers['user-agent'] , shortUrl, visitor: req.user.id , creator: result.creator});
			return res.redirect(result.longUrl);
		}
		res.status(result.status).json(result.body);
	})
	.catch(err=>{
		log('error', err);
		res.status(500).json({message: 'interval error'});
	})
})




module.exports = router;
