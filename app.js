require('dotenv').config();

const express = require('express');
const log = require('./logger/logger');
const cors = require('cors');
const path = require('path');
const mongoose = require('mongoose');
const passport = require("passport");
const bodyParser = require('body-parser');
const port = process.env.PORT || 8000;
const authenticate = require('./security/authenticate');

const app = express();

app.use(cors());
app.use(express.json());
app.use(express.static(path.join(__dirname, 'public')));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

app.use(passport.initialize());
require("./authentication/jwtauth")(passport);

const auth = require('./authentication/routes');
app.use('/api/auth', auth);

app.use(authenticate);

const user = require('./user/routes');
app.use('/api/users/', user);

const url = require('./url/routes');
app.use('/api/urls/', url);

const visit = require('./visit/routes');
app.use('/api/visits/', visit);

app.use((req, res) => {
	log('error', `url: ${req.url} not found.`);
	return res.status(404).json({message: `url: ${req.url} Not found.`});
});

const connectionString = `mongodb+srv://${process.env.DB_USERNAME}:${process.env.DB_PASS}@cluster0.rbxbu.mongodb.net/${process.env.DB_NAME}?retryWrites=true&w=majority`;
mongoose
	.connect(connectionString, {
		useNewUrlParser: true,
		useUnifiedTopology: true,
		useFindAndModify: false,
		useCreateIndex: true,
	})
	.then(() => {
		app.listen(port, function () {
			log('info', `app started at port ${port}`);
		});
	})
	.catch((err) => {
		log('error', err);
	});