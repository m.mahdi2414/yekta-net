const redis = require('redis');
const log = require('./../logger/logger');

const redis_client = redis.createClient(
	process.env.REDIS_PORT,
	process.env.REDIS_HOST
);
redis_client.auth(process.env.REDIS_PASSWORD);
redis_client.flushall();

let setInCache = (id, data) => {
	redis_client.setex(id, 3600, JSON.stringify(data));
};


let getFromCache = async (id) => {
	let promise = new Promise((resolve, reject) => {
		redis_client.get(id, (err, data) => {
			if (err) {
				reject(err);
				return;
			}
			if (data)
				resolve(data);
			else
				reject('not found in cache')
		});
	});
	return await promise;
};

let remove = (id)=>{
	redis_client.flushall();
}

module.exports = {setInCache, getFromCache , remove};
