const Url = require('./model');

let getUrl = async (shortUrl) =>{
    return await Url.findOne({shortUrl});
}

let createUrl = async (urlJson) => {
	const url = new Url(urlJson);
	return await url.save();
};

module.exports = {getUrl, createUrl};
