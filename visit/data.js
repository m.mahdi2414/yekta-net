const Visit = require('./model');
const mongoose = require('mongoose');

let createVisit = async (visitJson) => {
    let visit = new Visit(visitJson);
    return await visit.save();
};

let getAll = async (userId) =>{
    return await Visit.find({creator:mongoose.Types.ObjectId(userId)});
}


let toMilliseconds = (days) => (days *  24 * 60 * 60 * 1000);
let getMilliseconds = (now) => (now.getHours() * 3600 + now.getMinutes() * 60 + now.getSeconds()) * 1000 + now.getMilliseconds();

let getAnnalistic = async (userId) => {
    let now = new Date();
    let todayStart = new Date(now.getTime() - getMilliseconds(now));
    let lastDayStart = new Date(todayStart.getTime() - toMilliseconds(1));
    let lastWeekStart = new Date(todayStart.getTime() - toMilliseconds(7));
    let lastMonthStart = new Date(todayStart.getTime() - toMilliseconds(30));
    let todayMatch = { $match: { createAt: { $gte: todayStart }}};
    let lastDayMatch = { $match: { createAt:{$gte:lastDayStart, $lt: todayStart}}};
    let lastWeekMatch = { $match: { createAt:{$gte:lastWeekStart, $lt: todayStart}}};
    let lastMonthMatch = { $match: { createAt:{$gte:lastMonthStart, $lt: todayStart}}};
    let groupAll = {$group: {_id: {shortUrl: '$shortUrl'} ,count: {$sum: 1}}};
    let groupOnPc = {$group: {_id: {shortUrl: '$shortUrl', isPc: '$isPc'} ,count: {$sum: 1}}};
    let groupOnBrowser = {$group: {_id: {shortUrl: '$shortUrl', browser: '$browser'} ,count: {$sum: 1}}};
    let distinctGroupAll = {$group: {_id: {shortUrl: '$shortUrl', visitor: '$visitor'} ,count: {$sum: 1}}};
    let distinctGroupOnPc = {$group: {_id: {shortUrl: '$shortUrl', visitor: '$visitor', isPc: '$isPc'} ,count: {$sum: 1}}};
    let distinctGroupOnBrowser = {$group: {_id: {shortUrl: '$shortUrl', visitor: '$visitor', browser: '$browser'} ,count: {$sum: 1}}};
    return await Visit.aggregate([
        { 
            $match: { 
                creator: {
                    $in: [mongoose.Types.ObjectId(userId)]
                } 
            } 
        }
    ]).facet({
        allToday : [todayMatch,groupAll],
        onPcToday: [todayMatch,groupOnPc],
        onBrowserToday: [todayMatch,groupOnBrowser],
        allDistinctToday: [todayMatch,distinctGroupAll],
        onPcDistinctToday: [todayMatch,distinctGroupOnPc],
        onBrowserDistinctToday: [todayMatch,distinctGroupOnBrowser],

        allLastDay : [lastDayMatch,groupAll],
        onPcLastDay: [lastDayMatch,groupOnPc],
        onBrowserLastDay: [lastDayMatch,groupOnBrowser],
        allDistinctLastDay: [lastDayMatch,distinctGroupAll],
        onPcDistinctLastDay: [lastDayMatch,distinctGroupOnPc],
        onBrowserDistinctLastDay: [lastDayMatch,distinctGroupOnBrowser],

        allLastWeek : [lastWeekMatch,groupAll],
        onPcLastWeek: [lastWeekMatch,groupOnPc],
        onBrowserLastWeek: [lastWeekMatch,groupOnBrowser],
        allDistinctLastWeek: [lastWeekMatch,distinctGroupAll],
        onPcDistinctLastWeek: [lastWeekMatch,distinctGroupOnPc],
        onBrowserDistinctLastWeek: [lastWeekMatch,distinctGroupOnBrowser],

        allLastMonth : [lastMonthMatch,groupAll],
        onPcLastMonth: [lastMonthMatch,groupOnPc],
        onBrowserLastMonth: [lastMonthMatch,groupOnBrowser],
        allDistinctLastMonth: [lastMonthMatch,distinctGroupAll],
        onPcDistinctLastMonth: [lastMonthMatch,distinctGroupOnPc],
        onBrowserDistinctLastMonth: [lastMonthMatch,distinctGroupOnBrowser],
    });
};
module.exports = {createVisit, getAnnalistic, getAll};
