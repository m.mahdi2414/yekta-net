const express = require('express');
const log = require('./../logger/logger');
const router = express.Router();
const {createUser, login} = require('./../user/service');

router.use(function (req, res, next) {
	log('info', `new ${req.method} request on ${req.originalUrl}`);
	next();
});

router.post('/register', (req, res)=>{
    const user = req.body;
    createUser(user).then(result => {
        return res.status(result.status).json(result.body);
    })
    .catch(err=>{
        res.status(500).json({message: 'interval error'});
    })
});

router.post('/login', (req, res)=>{
    const user = req.body;
    login(user).then(result => {
        return res.status(result.status).json(result.body);
    })
    .catch(err=>{
        res.status(500).json({message: 'interval error'});
    })
})

module.exports = router;