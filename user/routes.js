const express = require('express');
const router = express.Router();

router.get('/me', (req, res) => {
	let result = req.user;
	return res.status(200).json(result);
});

module.exports = router;
