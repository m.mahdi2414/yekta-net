const express = require('express');
const router = express.Router();
const {getAnnalistic , getAll} = require('./service');
const log = require('../logger/logger');

router.use(function (req, res, next) {
	log('info', `new ${req.method} request on ${req.originalUrl}`);
	next();
});

router.get('/', (req , res)=>{
	getAll(req.user.id).then(result=>{
		res.status(result.status).json(result.body);
	}).catch(err=>{
		log('error' , err);
		res.status(500).json('interval error');
	})
});

router.get('/annalistic', (req , res)=>{
	getAnnalistic(req.user.id).then(result=>{
		res.status(result.status).json(result.body);
	}).catch(err=>{
		console.log(err);
		res.status(500).json('interval error');
	})
});

module.exports = router;