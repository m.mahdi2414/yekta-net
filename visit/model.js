const mongoose = require('mongoose');

const Schema = mongoose.Schema;
const visitSchema = new Schema({
    shortUrl: {
        type: String,
        ref: 'Url',
        required: true,
    },
    creator: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true,
    },
    visitor:{
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true,
    },
    browser: {
        type: String,
        required: true,
    },
    isPc: {
        type: Boolean,
        required: true,
    },
    createAt:{
        type: Date, 
        required: true, 
        default: Date.now,
        expires: 2678400,
    },
},
{
    toJSON: {
        transform: function(doc, ret) {
            ret.id = ret._id;
            delete ret._id;
            delete ret.__v;
        },
    },
});

module.exports = mongoose.model('Visit', visitSchema);
