const {getUserRoles} = require('./../user/service');

module.exports = function permit(...allowed) {
	return (req, res, next) => {
		if (allowed.includes(req.user.role.name)) {
			next();
			return;
		}	
		res.status(401).json({
			message: `no permission for ${req.originalUrl}`,
		});
	};
};
