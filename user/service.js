const log = require('../logger/logger');
const data = require('./data');


let createUser = async (user) => {

    let promise = new Promise((resolve, reject)=>{
        data.createUser(user).then((user)=>{
            log('info', `user ${user.toJSON().username} created`);
            resolve({body:{token: user.generateJWT(), user: user.toJSON()}, status: 200});
        })
        .catch((err) => {
            log('error', JSON.stringify(err));
            if (err.code === 11000) {
                resolve({body: {message: 'duplicate user'}, status: 400});
                return;
            }
            resolve({body: err, status: 422});
        });
    });
    return await promise;
}



let login = async (userInfo) => {
    let userToFind;
    if (userInfo.username.includes('@')) {
        userToFind = {email: userInfo.username};
    }
    else {
        userToFind = {username: userInfo.username};
    }
    let promise = new Promise((resolve, reject) => {
        data.getUser(userToFind).then(user=>{
            if (user){
                if (!user.comparePassword(userInfo.password)){
                    message ='Invalid password';
                    log('info' , message);
                    resolve({status:400,body:{message}});
                    return;
                }
                resolve({body:{token: user.generateJWT(), user: user.toJSON()}, status: 200});
            }
            else {
                let message = `The user info is not associated with any account. Double-check your email address or username and try again.`;
                log('info', message);
                resolve({status:401, body: {message}});
            }
        }).catch(err => {
            log('error', err);
			resolve({body: err, status: 422});
        });
    });
    return await promise;
}

module.exports = {createUser, login};