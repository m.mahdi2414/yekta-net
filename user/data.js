const User = require('./model');

let getUser = async (user) =>{
    return await User.findOne(user);
}

let createUser = async (userJson) => {
	const user = new User(userJson);
	return await user.save();
};

module.exports = {getUser, createUser};
