const log = require('../logger/logger');
const data = require('./data');
const useragent = require('useragent');
let createVisit = async ({userAgent , shortUrl, visitor , creator}) => { 
    const agent = useragent.parse(userAgent);
    const device = agent.device.family;
    const browser = agent.family;
    data.createVisit({browser, visitor, shortUrl, creator, isPc: device==='Other'}).then(result=>{
        log('info', 'visit created');
    }).catch(err => {
        log('error', err);
    });
}


let getAnnalistic = async (userId) => {
    let promise = new Promise((resolve, reject) => {
        data.getAnnalistic(userId).then(result=>{
            resolve({status:200 , body: result});
        })
        .catch(err=>{
            reject({status:500, body:err});
        });
    });
    return await promise;
}

let getAll = async (userId) => {
    let promise = new Promise((resolve, reject) => {
        data.getAll(userId).then(result=>{
            resolve({status:200 , body: result});
        })
        .catch(err=>{
            reject({status:500, body:err});
        });
    });
    return await promise;
}
module.exports = {createVisit, getAnnalistic, getAll};