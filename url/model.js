const mongoose = require('mongoose');

const Schema = mongoose.Schema;
const urlSchema = new Schema({
    userId: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true,
    },
    shortUrl: {
        type: Schema.Types.Mixed,
        required: true,
        index:{
            unique: true
        }
    },
    longUrl: {
        type: String,
        required: true,
    },
},
{
    toJSON: {
        transform: function(doc, ret) {
            ret.id = ret._id;
            delete ret._id;
            delete ret.__v;
        },
    },
});

module.exports = mongoose.model('Url', urlSchema);
